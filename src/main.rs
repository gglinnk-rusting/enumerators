#![allow(unused)]

enum IPAddrKind {
    V4,
    V6,
}

struct IPAddr {
    kind: IPAddrKind,
    address: String,
}

enum NewIPAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn main() {
    let v_four = IPAddrKind::V4;
    let v_six = IPAddrKind::V6;
    let some = Some("THAT A STRING !");
    let empty: Option<i32> = None;

    let home = IPAddr {
        kind: IPAddrKind::V4,
        address: String::from("127.0.0.1"),
    };
    let loopback = IPAddr {
        kind: IPAddrKind::V6,
        address: String::from("::1"),
    };

    let new_home = NewIPAddr::V4(127, 0, 0, 1);
    let new_loopback = NewIPAddr::V6(String::from("::1"));

    route(IPAddrKind::V4);
    route(IPAddrKind::V6);
}

fn route(ip_kind: IPAddrKind) {}
